#!/bin/bash

# taken from http://www.sqlite.org/cvstrac/wiki?p=HowToCompile

  rm tclsqlite.c
  for i in *.c; do gcc -O -c $i; done
  rm shell.o
  ar cru libsqlite.a *.o
  ranlib libsqlite.a
  gcc -o sqlite3 shell.c libsqlite.a  -ldl -lpthread  -O2


