/* 
 * File:   example3.cc
 * Author: debian
 *
 * Created on October 27, 2014, 4:43 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>

#include "Property.h"

#include <DatabaseSQL.h>

#include <tools/cpu-time.hpp>

using namespace std;



/*
 * 
 */

 struct Hiberlite {

        void createDB() {

            Database db("property.db");
            db.registerBeanClass<Property>();
            vector<string> msg=db.checkModel();
            for(size_t ci=0;ci<msg.size();ci++)
                    cout << "model check reported: " << msg[ci] << endl;

            db.dropModel();
            db.createModel();
            
            
            string STRING;
            ifstream infile;
            
            double wall00 = tools::get_wall_time();
            double wall02,wall03;
            
            infile.open ("/usr/share/dict/american-english");
            Property prop;
            char buf[10];
            unsigned int i=0;
            if (infile.is_open())
            while(!infile.eof()) // To get you all the lines.
            {
                if ( i>3000) break;
	        getline(infile,STRING); // Saves the line in STRING.                
                prop.type = prop.getType();
                prop.value = STRING;
                sprintf(buf,"id_%d",i++);
                prop.booking_id = string(buf);
                prop.follower =prop.getFollower();
                prop.distance = prop.getDistance();
                
//	        cout<<prop; // Prints our Property.
                 wall02 = tools::get_wall_time();  
                 db.copyBean(prop); // put sqlite   
                 wall03 = tools::get_wall_time();
                  if ( i%100 == 0 ) {
                        cout<<"Num calls "<<i<<"\t"<<"Time elapsed (s): "<<wall03 - wall00<<"\n";
                        cout<<"Inserting (s) "<< wall03 -wall02<<"\n";
                 }
            }
           infile.close();
            

        }
        
         void createDB2() {
             
             /*
              
    public:
    string type;
    string value; 
    string  booking_id;
    float distance;
    string follower;
    

              */
             
             
            DatabaseSQL *db;
            db = new DatabaseSQL("property_v2.db");
            
            db->query("CREATE TABLE property (type TEXT, value TEXT, booking_id TEXT, distance FLOAT, follower TEXT);");


            
             string STRING;
            ifstream infile;
            
            double wall00 = tools::get_wall_time();
            double wall02,wall03;
            
            infile.open ("/usr/share/dict/american-english");
            Property prop;
            string cm("\"");
            char buf[10];
            unsigned int i=0;
            if (infile.is_open())
            while(!infile.eof()) // To get you all the lines.
            {
                if ( i>3000) break;                
	        getline(infile,STRING); // Saves the line in STRING.    
                sprintf(buf,"id_%d",i++);

                string query=  "INSERT INTO property VALUES(";
                query +=  cm+prop.getType()+cm + "," + cm+STRING+cm + "," + cm+string(buf)+cm + ",";
                sprintf(buf,"%f",prop.getDistance());
                query += cm+string(buf)+cm + "," + cm+prop.getFollower()+cm + ");";
                wall02 = tools::get_wall_time();                 
                db->query(const_cast<char*>(query.c_str()));
                wall03 = tools::get_wall_time();                
                
                 if ( i%100 == 0 ) {
                        cout<<"Num calls "<<i<<"\t"<<"Time elapsed (s): "<<wall03 - wall00<<"\n";
                        cout<<"Inserting (s) "<< wall03 -wall02<<"\n";
                 }
            }
           infile.close();
            
            
            db->close();
            delete db;
         }
            
            
    };

int main(int argc, char** argv) {

    Hiberlite tester;
    cout<<"\n\n\n\n Test 1\n\n\n\n";
    tester.createDB();    
    cout<<"\n\n\n\n Test 2\n\n\n\n";
    tester.createDB2();
    
 
    return 0;
}

