/* 
 * File:   example5.cc
 * Author: debian
 *
 * Created on October 28, 2014, 1:58 PM
 */

#include <cstdlib>

#include "Property2.h"
#include <tools/cpu-time.hpp>
#include <tools/vprintf.hpp>
#include <iostream>
#include <fstream>

using namespace std;

/*
 * 
 */
struct Test {

    // to test creation and filling the database with SOCI framework
        void createDB() {

            DatabaseSOCI *db;
            db = new DatabaseSOCI("property_v4.db");                        
            db->query(tools::vprintf("CREATE TABLE %s (TYPE  TEXT, VALUE TEXT, BOOKINGID TEXT, DISTANCE DOUBLE, FOLLOWER TEXT);", "property2").c_str());

            string STRING;
            ifstream infile;
            
            double wall00 = tools::get_wall_time();
            double wall02,wall03;
            
            infile.open ("/usr/share/dict/american-english");
            Property2 prop;
            unsigned int i=0;
            if (infile.is_open())
            while(!infile.eof()) // To get you all the lines.
            {
                if ( i>3000) break;
	        getline(infile,STRING); // Saves the line in STRING.                
                prop.type = prop.getType();
                prop.value = STRING;
                prop.booking_id = tools::vprintf("id_%d",i++);
                prop.follower =prop.getFollower();
                prop.distance = prop.getDistance();
                
//	        cout<<prop; // Prints our Property .
                 wall02 = tools::get_wall_time();  

                 db->queryInsert("","property2","TYPE,VALUE,BOOKINGID,DISTANCE,FOLLOWER",prop);

                 wall03 = tools::get_wall_time();
                  if ( i%100 == 0 ) {
                        cout<<"Num calls "<<i<<"\t"<<"Time elapsed (s): "<<wall03 - wall00<<"\n";
                        cout<<"Inserting (s) "<< wall03 -wall02<<"\n";
                 }
            }
           infile.close();
           delete db;
            
        }
        
        // to test the selection query: boolean expression
        void query1() {
            DatabaseSOCI *db;
            db = new DatabaseSOCI("property_v4.db");                        
            Property2 prop;
            double wall00 = tools::get_wall_time();
            double wall02;       
            string query =  tools::vprintf("SELECT * FROM %s WHERE DISTANCE > %f","property2",7.0);
            vector<Property2> result = db->querySelect(query.c_str(),"","",prop);
            wall02 = tools::get_wall_time();
            for(size_t j=0;j<result.size();j++){
                cout<<result[j];
            }            
            cout<<"querying  \t\t"<<query<<"\nrequires (s)"<< wall02 -wall00<<"\n";
            
            delete db;
        }
        
        // to test the selection query: like expression        
        void query2() {
            DatabaseSOCI *db;
            db = new DatabaseSOCI("property_v4.db");                        
            Property2 prop;
           double wall00 = tools::get_wall_time();
            double wall02;
            string query =  tools::vprintf("SELECT * FROM %s WHERE VALUE LIKE %s","property2","\'%li%\'");
            vector<Property2> result = db->querySelect( query.c_str(),"","",prop);
             wall02 = tools::get_wall_time();
            for(size_t j=0;j<result.size();j++){
                cout<<result[j];
            }
              cout<<"querying  \t\t"<<query<<"\nrequires (s) "<< wall02 -wall00<<"\n";
            delete db;
        }

                // to test the selection query: multiple conditions 
        void query3() {
            DatabaseSOCI *db;
            db = new DatabaseSOCI("property_v4.db");                        
            Property2 prop;
           double wall00 = tools::get_wall_time();
            double wall02;
            string query =  tools::vprintf("SELECT * FROM %s WHERE VALUE LIKE %s AND DISTANCE BETWEEN %f AND %f","property2","\'%li%\'",2.5,4.0);
            vector<Property2> result = db->querySelect( query.c_str(),"","",prop);
             wall02 = tools::get_wall_time();
            for(size_t j=0;j<result.size();j++){
                cout<<result[j];
            }
              cout<<"querying  \t\t"<<query<<"\nrequires (s) "<< wall02 -wall00<<"\n";
            delete db;
        }

        
    };

int main(int argc, char** argv) {


    Test tester;
    
    cout<<"\n\n\n\n Test 1\n\n\n\n";
    //tester.createDB();
    cout<<"\n\n\n\n Test 2\n\n\n\n";
    tester.query1();
    cout<<"\n\n\n\n Test 3\n\n\n\n";
    tester.query2();
    cout<<"\n\n\n\n Test 4\n\n\n\n";
    tester.query3();
    
    
    return 0;
}