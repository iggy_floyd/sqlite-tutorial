/* 
 * File:   example6.cc
 * Author: debian
 *
 * Created on October 28, 2014, 3:59 PM
 */

#include <cstdlib>



#include "Property2.h"




#include <tools/cpu-time.hpp>
#include <tools/vprintf.hpp>
#include <iostream>
#include <fstream>


using namespace std;


/*
 * 
 */

struct Test {

    // to test creation and filling the database with SOCI framework
        void createDB() {

            DatabaseSOCI *db;
            db = new DatabaseSOCI("property_v5.db");                        
            db->query(tools::vprintf("CREATE TABLE %s (TYPE  TEXT, VALUE TEXT, BOOKINGID TEXT, DISTANCE DOUBLE, FOLLOWER TEXT);", "property2").c_str());

            string STRING;
            ifstream infile;
            
            double wall00 = tools::get_wall_time();
            double wall02,wall03;
            
            infile.open ("/usr/share/dict/american-english");
            Property2 prop;
            unsigned int i=0;
            if (infile.is_open())
            while(!infile.eof()) // To get you all the lines.
            {
                if ( i>3000) break;
	        getline(infile,STRING); // Saves the line in STRING.                
                prop.type = prop.getType();
                prop.value = STRING;
                prop.booking_id = tools::vprintf("id_%d",i++);
                prop.follower =prop.getFollower();
                prop.distance = prop.getDistance();
                
//	        cout<<prop; // Prints our Property .
                 wall02 = tools::get_wall_time();  

                 db->queryInsert("","property2","TYPE,VALUE,BOOKINGID,DISTANCE,FOLLOWER",prop);

                 wall03 = tools::get_wall_time();
                  if ( i%100 == 0 ) {
                        cout<<"Num calls "<<i<<"\t"<<"Time elapsed (s): "<<wall03 - wall00<<"\n";
                        cout<<"Inserting (s) "<< wall03 -wall02<<"\n";
                 }
            }
           infile.close();
           delete db;            
             
        }
        
         void createDB2() {
             /*
            DatabaseSQL *db;
            db = new DatabaseSQL("property_v6.db");

            db->query("CREATE TABLE property (type TEXT, value TEXT, booking_id TEXT, distance FLOAT, follower TEXT);");
            string STRING;
            ifstream infile;
            
            double wall00 = tools::get_wall_time();
            double wall02,wall03;
            
            infile.open ("/usr/share/dict/american-english");
            Property2 prop;
            string cm("\"");
            char buf[10];
            unsigned int i=0;
            if (infile.is_open())
            while(!infile.eof()) // To get you all the lines.
            {
                if ( i>3000) break;                
	        getline(infile,STRING); // Saves the line in STRING.    
                sprintf(buf,"id_%d",i++);

                string query=  "INSERT INTO property VALUES(";
                query +=  cm+prop.getType()+cm + "," + cm+STRING+cm + "," + cm+string(buf)+cm + ",";
                sprintf(buf,"%f",prop.getDistance());
                query += cm+string(buf)+cm + "," + cm+prop.getFollower()+cm + ");";
                wall02 = tools::get_wall_time();                 
                db->query(const_cast<char*>(query.c_str()));
                wall03 = tools::get_wall_time();                
                
                 if ( i%100 == 0 ) {
                        cout<<"Num calls "<<i<<"\t"<<"Time elapsed (s): "<<wall03 - wall00<<"\n";
                        cout<<"Inserting (s) "<< wall03 -wall02<<"\n";
                 }
            }
           infile.close();
            
            
            db->close();
            delete db;
  */            
         }
};

int main(int argc, char** argv) {

    Test tester;
    cout<<"\n\n\n\n Test 1\n\n\n\n";
    tester.createDB();
    cout<<"\n\n\n\n Test 2\n\n\n\n";
    tester.createDB2();
    
    return 0;
}

