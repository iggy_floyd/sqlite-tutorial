/* 
 * File:   example4.cc
 * Author: debian
 *
 * Created on October 27, 2014, 6:56 PM
 */

#include <cstdlib>
#include <iostream>
#include <string>

#include "DatabaseSOCI.h"


//#include "soci.h"
//#include "soci-sqlite3.h" 

using namespace std;

/*
 * 
 */

using namespace soci;




int main(int argc, char** argv) {


    
    DatabaseSOCI *db;
    db = new DatabaseSOCI("property_v3.db");
    db->query("create table soci_test( id integer, name varchar, subname varchar);");
    db->query("insert into soci_test(id,name,subname) values( 1,'john','smith');");
    db->query("insert into soci_test(id,name,subname) values( 2,'george','vals');");
    db->query("insert into soci_test(id,name,subname) values( 3,'ann','smith');");
    db->query("insert into soci_test(id,name,subname) values( 4,'john','grey');");
    db->query("insert into soci_test(id,name,subname) values( 5,'anthony','wall');");

    vector<vector<string> > result = db->query("SELECT * FROM soci_test;");
    for(vector<vector<string> >::iterator it = result.begin(); it < result.end(); ++it)
    {
	vector<string> row = *it;
        vector<string>::iterator jt;
        std::ostringstream doc;
        for (jt = row.begin();jt!=row.end();++jt) 
            doc<<*jt<<"\t";
            cout << doc.str() << endl;
    }

    
    
    delete db;
    return 0;    
}

