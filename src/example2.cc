/**
 * 
 * example2 show a use of a simple ORM C++ (hiberlite) for sqlite3 
 * 
 * 
 */


#include <Person.h>




using namespace std;



int main() {
    
    struct Hiberlite {

        void create() {

            Database db("person.db");
            db.registerBeanClass<Person>();
            vector<string> msg=db.checkModel();
            for(size_t ci=0;ci<msg.size();ci++)
                    cout << "model check reported: " << msg[ci] << endl;

            db.dropModel();
            db.createModel();

            for(int i=0;i<5;i++){
                char ss[100];
                Person obj;
                sprintf(ss,"Igor Marfin %d",i);
                obj.name = ss;
                obj.age = 30 + i;
                obj.height = 175; // cm
                obj.weight = 70.5; // kg
                obj.children["alisa"] = 3+i; // 3 years old
                obj.children["arina"] = 7+i; // 7 years old
                for (int j=0;j<3; j++) obj.salary.push_back((i+1)*3000.+j);
                db.copyBean(obj); // put sqlite                           
            }

        }
            void readall() {
                 Database db("person.db");

                 vector< hiberlite::bean_ptr<Person> > v=db.getAllBeans<Person>();
                 cout << "found " << v.size() << " persons in the database:\n";

                 for(size_t j=0;j<v.size();j++){
                        cout << "[name=" << v[j]->name << "\t";
                        cout << "age=" << v[j]->age << "\t";
                        cout << "weight=" << v[j]->weight << "\t";
                        cout << "height=" << v[j]->height << "\t";
                        cout << "salary= {";
                        for (std::vector<float>::iterator it = v[j]->salary.begin(); it != v[j]->salary.end(); it++)
                        cout<<*it<<" €\t";               
                        cout<<"}\t";               
                        cout << "children={";
                        for (std::map<string,int>::iterator it = v[j]->children.begin(); it != v[j]->children.end(); it++)
                            cout<<it->first <<" : "<<it->second<<" years old\t";                        
                            cout << "}]\n";
                   }

            }

            void query1() {
                 Database db("person.db");

                 std::string query = "age > 32";
                 
                 vector< hiberlite::bean_ptr<Person> > v=db.getAllBeans<Person>(query,"");
                 cout << "found " << v.size() << " persons in the database:\n";

                 for(size_t j=0;j<v.size();j++){
                        cout << "[name=" << v[j]->name << "\t";
                        cout << "age=" << v[j]->age << "\t";
                        cout << "weight=" << v[j]->weight << "\t";
                        cout << "height=" << v[j]->height << "\t";
                        cout << "salary= {";
                        for (std::vector<float>::iterator it = v[j]->salary.begin(); it != v[j]->salary.end(); it++)
                        cout<<*it<<" €\t";               
                        cout<<"}\t";               
                        cout << "children={";
                        for (std::map<string,int>::iterator it = v[j]->children.begin(); it != v[j]->children.end(); it++)
                            cout<<it->first <<" : "<<it->second<<" years old\t";                        
                            cout << "}]\n";
                   }

            }
            

            void query2(std::string key,std::string keyname,std::string val,std::string like = std::string("")) {
                 Database db("person.db");
          
                 //  SELECT * FROM Person WHERE EXISTS (SELECT hiberlite_parent_id FROM Person_salary_items WHERE  Person_salary_items.hiberlite_parent_id = Person.hiberlite_id and Person_salary_items.item > 12000);

                 std::string _and = std::string(" and ");
                 std::string _sp = std::string(" ");
                 std::string _fieldsp =std::string("_");
                 std::string table = db.getClassName<Person>();                 
                 std::string propertyName = _fieldsp + key + _fieldsp + "items";
                 
                 
                 std::string query = std::string("age > 33");
                 query+=_and+std::string("EXISTS ( SELECT ") + HIBERLITE_PARENTID_COLUMN + " FROM "+ table+propertyName+" WHERE ";
                 query+=_sp+table+propertyName+"."+HIBERLITE_PARENTID_COLUMN+"="+table+"."+HIBERLITE_PRIMARY_KEY_COLUMN;
                 if (like.size() and like == std::string("like"))
                    query+=_and +  table+propertyName+"." +keyname +_sp +like +_sp + val;
                 else                     
                    query+=_and + table+propertyName+"." +keyname + val;

                 query+=_sp+")";


                 
                 vector< hiberlite::bean_ptr<Person> > v=db.getAllBeans<Person>(query,"");
                 cout << "found " << v.size() << " persons in the database:\n";

                 for(size_t j=0;j<v.size();j++){
                        cout << "[name=" << v[j]->name << "\t";
                        cout << "age=" << v[j]->age << "\t";
                        cout << "weight=" << v[j]->weight << "\t";
                        cout << "height=" << v[j]->height << "\t";
                        cout << "salary= {";
                        for (std::vector<float>::iterator it = v[j]->salary.begin(); it != v[j]->salary.end(); it++)
                        cout<<*it<<" €\t";               
                        cout<<"}\t";               
                        cout << "children={";
                        for (std::map<string,int>::iterator it = v[j]->children.begin(); it != v[j]->children.end(); it++)
                            cout<<it->first <<" : "<<it->second<<" years old\t";                        
                            cout << "}]\n";
                   }

            }
            
            
        
             void query3(std::string key,std::string keyname,std::string val,std::string like = std::string("")) {
                 Database db("person.db");
                 db.registerBeanClass<Person>();
                 vector<string> msg=db.checkModel();
                 for(size_t ci=0;ci<msg.size();ci++)
                    cout << "model check reported: " << msg[ci] << endl;
          

                 //  SELECT * FROM Person WHERE EXISTS (SELECT hiberlite_parent_id FROM Person_salary_items WHERE  Person_salary_items.hiberlite_parent_id = Person.hiberlite_id and Person_salary_items.item > 12000);

                 std::string _and = std::string(" and ");
                 std::string _sp = std::string(" ");
                 std::string _fieldsp =std::string("_");
                 std::string table = db.getClassName<Person>();                 
                 std::string propertyName = _fieldsp + key + _fieldsp + "items";
                 
                 db.createIndex(table,"age_ind","age");
                 
                 
                 std::string query = std::string("age > 33");
                 query+=_and+std::string("EXISTS ( SELECT ") + HIBERLITE_PARENTID_COLUMN + " FROM "+ table+propertyName+" WHERE ";
                 query+=_sp+table+propertyName+"."+HIBERLITE_PARENTID_COLUMN+"="+table+"."+HIBERLITE_PRIMARY_KEY_COLUMN;
                 if (like.size() and like == std::string("like"))
                    query+=_and +  table+propertyName+"." +keyname +_sp +like +_sp + val;
                 else                     
                    query+=_and + table+propertyName+"." +keyname + val;

                 query+=_sp+")";


                 
                 vector< hiberlite::bean_ptr<Person> > v=db.getAllBeans<Person>(query,"");
                 cout << "found " << v.size() << " persons in the database:\n";

                 for(size_t j=0;j<v.size();j++){
                        cout << "[name=" << v[j]->name << "\t";
                        cout << "age=" << v[j]->age << "\t";
                        cout << "weight=" << v[j]->weight << "\t";
                        cout << "height=" << v[j]->height << "\t";
                        cout << "salary= {";
                        for (std::vector<float>::iterator it = v[j]->salary.begin(); it != v[j]->salary.end(); it++)
                        cout<<*it<<" €\t";               
                        cout<<"}\t";               
                        cout << "children={";
                        for (std::map<string,int>::iterator it = v[j]->children.begin(); it != v[j]->children.end(); it++)
                            cout<<it->first <<" : "<<it->second<<" years old\t";                        
                            cout << "}]\n";
                   }

            }
            
            
    };

    
// Hiberlite ORM
Hiberlite hl;
// a test of creation of the model
hl.create();
// a test of a simple fetch of all stored objects
hl.readall();
// a test of a simple sql query
hl.query1();
// tests of  complex sql queries 
hl.query2("salary","item",">1200");
hl.query2("children","item_second","=11");
hl.query2("children","item_first","\'ari%\'","like");
// a test of indexing and a complex sql query
hl.query3("children","item_first","\'ari%\'","like");



return 0;
}
