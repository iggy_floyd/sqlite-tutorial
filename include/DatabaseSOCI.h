/* 
 * File:   DatabaseSOCI.h
 * Author: debian
 *
 * Created on October 28, 2014, 8:13 AM
 */

#ifndef DATABASESOCI_H
#define	DATABASESOCI_H


#include <string>
#include <vector>
#include <iostream>



#include "soci.h"
#include "soci-sqlite3.h" 


using namespace soci;
using namespace std;



class DatabaseSOCI
{
public:
	DatabaseSOCI(char* filename):backEnd(*soci::factory_sqlite3())/*,sql(this->backEnd, string(filename))*/ {             
            sql.open(backEnd,string(filename));
        }
	~DatabaseSOCI() {};
	
        session & getConnector() { return sql;}
        
	template <class T> 
        void queryInsert(const char* query,const char* table,const char *params,T&); // dangerous!   The method should be implemented somehow in a different *.h or *.cc later!
        
        template <class T> 
        vector<T> querySelect(const char* query,const char* table, const char* where, T&); // dangerous!   The method should be implemented somehow in a different *.h or *.cc later!
        
        
        
	vector<vector<string> > query(const char* query);

	
	
private:
      session sql;
      backend_factory const &backEnd;
};



#ifndef DATABASECOCICPP




vector<vector<string> > DatabaseSOCI::query(const char* query)
{

    rowset<row> rs = (sql.prepare << query);        
    vector<vector<string> > results;
    
    
    
// iteration through the resultset:
for (rowset<row>::const_iterator it = rs.begin(); it != rs.end(); ++it)
{
    row const& row = *it;

    vector<string> values;
    for(std::size_t i = 0; i < row.size(); ++i)
    {
        std::ostringstream doc;
        
        const column_properties & props = row.get_properties(i);

        switch(props.get_data_type())
        {
            case dt_string:
                doc << row.get<std::string>(i);
                break;
            case dt_double:
                doc << row.get<double>(i);
                break;
            case dt_integer:
                doc << row.get<int>(i);
                break;
            case dt_long_long:
                doc << row.get<long long>(i);
                break;
            case dt_unsigned_long_long:
                doc << row.get<unsigned long long>(i);
                break;
            case dt_date:
                std::tm when = row.get<std::tm>(i);
                doc << asctime(&when);
                break;
        }
        values.push_back(doc.str());
        
    }
    results.push_back(values);
    
}
    return results;
}




#endif

#endif	/* DATABASESOCI_H */

