/* 
 * File:   Person.h
 * Author: debian
 *
 * Created on October 24, 2014, 4:09 PM
 */



#ifndef PERSON_H
#define	PERSON_H



#include "hiberlite.h"
using namespace hiberlite;

#include <iostream>
#include <cmath>
#include <vector>
#include <map>
using namespace std;




class Person{
        friend class hiberlite::access;
        template<class Archive>
        void hibernate(Archive & ar)
        {
                ar & HIBERLITE_NVP(name);
                ar & HIBERLITE_NVP(weight);
                ar & HIBERLITE_NVP(age);
                ar & HIBERLITE_NVP(height);
                ar & HIBERLITE_NVP(children);
                ar & HIBERLITE_NVP(salary);
        }

        public:
                string name;
                int age;
                double weight;
                int height;
                map<string,int > children;
                vector<float> salary;
};



HIBERLITE_EXPORT_CLASS(Person)



#endif	/* PERSON_H */

