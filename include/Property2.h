/* 
 * File:   Property2.h
 * Author: debian
 *
 * Created on October 28, 2014, 1:59 PM
 */

#ifndef PROPERTY2_H
#define	PROPERTY2_H

#include <iostream>
#include <cmath>
#include <vector>
#include <map>
#include <string>
#include <stdlib.h>     /* srand, rand */
using namespace std;


#include "DatabaseSOCI.h"


class Property2 {
    

public:    
    Property2() {
        
        _typemap[BOOKER] = "BOOKER";
        _typemap[TRAVELLER] = "TRAVELLER";
        _typemap[EMAIL] = "EMAIL";
        _typemap[PHONE] = "PHONE";
        _typemap[BOOKING] = "BOOKING";
        
        _followermap[ME] = "ME";
        _followermap[YOU] = "YOU";
        _followermap[GERMAN] = "GERMAN";
        _followermap[ENGLISH] = "ENGLISH";
        _followermap[SOMEBODYELSE] = "SOMEBODYELSE";
    }
    
        

public:
    string type;
    string value; 
    string  booking_id;
    double distance;
    string follower;
    
    // a help enumeration to define possible types of the property
    enum types {
        BOOKER,
        TRAVELLER,
        EMAIL,
        PHONE,
        BOOKING,
        lasttypes
    };

        // a help enumeration to define possible followers of the property (of course it does not correspond to reality)
    enum followers {
        ME,
        YOU,
        GERMAN,
        ENGLISH,
        SOMEBODYELSE,
        lastfollowers
    };
    
    
    // return randomly chosen types and followers
    string getType() { return  _typemap[static_cast<types>(rand() % lasttypes)];}
    string getFollower() { return  _followermap[static_cast<followers>(rand() % lastfollowers)];}
    float getDistance() { return  rand()  % 10 ;}

  
  friend std::ostream& operator<< (std::ostream& o, Property2 const& prop);
  
  
  private:

    std::map<types,string> _typemap;
    std::map<followers,string> _followermap;
    
};


std::ostream& operator<< (std::ostream& o, Property2 const& prop)
{
  return o <<"{Type=" <<prop.type<<"\t"
          <<"Value="<<prop.value<<"\t"
          <<"Booking_id="<<prop.booking_id<<"\t"
          <<"Distance="<<prop.distance<<"\t"
          <<"Follower="<<prop.follower<<"}\n";
          
}


// here the SOCI ORM on Property2 is implemented

namespace soci
{
    template<>
    struct type_conversion<Property2>
    {
        typedef values base_type;

        static void from_base(values const & v, indicator  ind , Property2 & p)
        {
            p.type = v.get<string>("TYPE");
            p.value = v.get<string>("VALUE");
            p.booking_id = v.get<string>("BOOKINGID");
            p.distance = v.get<double>("DISTANCE",-1.0);
            p.follower  =  v.get<string>("FOLLOWER","");

        }
    
        static void to_base(const Property2 & p, values & v, indicator & ind)
        {
            v.set("TYPE", p.type);
            v.set("VALUE", p.value);
            v.set("BOOKINGID", p.booking_id);
            v.set("DISTANCE", p.distance);
            v.set("FOLLOWER", p.follower, p.follower.empty() ? i_null : i_ok);
            ind = i_ok;
        }
    };
}


// here we extend DatabaseSOCI to work with the Property2 class

template<>
void DatabaseSOCI::queryInsert<Property2>(const char* query,const char* table,const char *params,Property2& prop) {
    
    sql << (string("insert into ") + string(table) + "(" +  string(params) + ")").c_str()
        << "values(:TYPE,:VALUE,:BOOKINGID,:DISTANCE,:FOLLOWER)", use (prop);                
    
}


template<>
vector<Property2> DatabaseSOCI::querySelect<Property2>(const char* query,const char* table,const char *where,Property2& prop) {
    
    vector<Property2> result;
    statement st = (sql.prepare << query
                                <<where,into(prop));
    
    st.execute();
    while (st.fetch()) {
        result.push_back(prop);
    }
                
    return result;
}



#endif	/* PROPERTY2_H */

