/* 
 * File:   Property.h
 * Author: debian
 *
 * Created on October 27, 2014, 4:20 PM
 */

#ifndef PROPERTY_H
#define	PROPERTY_H


#include "hiberlite.h"
using namespace hiberlite;

#include <iostream>
#include <cmath>
#include <vector>
#include <map>
#include <string>
#include <stdlib.h>     /* srand, rand */
using namespace std;





class Property {
    
    friend class hiberlite::access;
        template<class Archive>
        void hibernate(Archive & ar)
        {
                ar & HIBERLITE_NVP(type);
                ar & HIBERLITE_NVP(value);
                ar & HIBERLITE_NVP(booking_id);
                ar & HIBERLITE_NVP(distance);
                ar & HIBERLITE_NVP(follower);
        }

public:    
    Property() {
        
        _typemap[BOOKER] = "BOOKER";
        _typemap[TRAVELLER] = "TRAVELLER";
        _typemap[EMAIL] = "EMAIL";
        _typemap[PHONE] = "PHONE";
        _typemap[BOOKING] = "BOOKING";
        
        _followermap[ME] = "ME";
        _followermap[YOU] = "YOU";
        _followermap[GERMAN] = "GERMAN";
        _followermap[ENGLISH] = "ENGLISH";
        _followermap[SOMEBODYELSE] = "SOMEBODYELSE";
    }
    
        

public:
    string type;
    string value; 
    string  booking_id;
    float distance;
    string follower;
    
    // a help enumeration to define possible types of the property
    enum types {
        BOOKER,
        TRAVELLER,
        EMAIL,
        PHONE,
        BOOKING,
        lasttypes
    };

        // a help enumeration to define possible followers of the property (of course it does not correspond to reality)
    enum followers {
        ME,
        YOU,
        GERMAN,
        ENGLISH,
        SOMEBODYELSE,
        lastfollowers
    };
    
    
    // return randomly chosen types and followers
    string getType() { return  _typemap[static_cast<types>(rand() % lasttypes)];}
    string getFollower() { return  _followermap[static_cast<followers>(rand() % lastfollowers)];}
    float getDistance() { return  rand()  % 10 ;}

  
  friend std::ostream& operator<< (std::ostream& o, Property const& prop);
  
  
  private:

    std::map<types,string> _typemap;
    std::map<followers,string> _followermap;
    
};

std::ostream& operator<< (std::ostream& o, Property const& prop)
{
  return o <<"{Type=" <<prop.type<<"\t"
          <<"Value="<<prop.value<<"\t"
          <<"Booking_id="<<prop.booking_id<<"\t"
          <<"Distance="<<prop.distance<<"\t"
          <<"Follower="<<prop.follower<<"}\n";
          
}




HIBERLITE_EXPORT_CLASS(Property)


#endif	/* PROPERTY_H */

