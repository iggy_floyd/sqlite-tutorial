/* 
 * File:   Person2.h
 * Author: debian
 *
 * Created on October 28, 2014, 2:56 PM
 */

#ifndef PERSON2_H
#define	PERSON2_H

#include <iostream>
#include <cmath>
#include <vector>
#include <map>
#include <string>


#include "DatabaseSOCI.h"

class Person2
{
    
public:
    int id;
    std::string firstName;
    std::string lastName;
    std::string gender;
};

namespace soci
{
    template<>
    struct type_conversion<Person2>
    {
        typedef values base_type;

        static void from_base(values const & v, indicator /* ind */, Person2 & p)
        {
            p.id = v.get<int>("ID");
            p.firstName = v.get<std::string>("FIRST_NAME");
            p.lastName = v.get<std::string>("LAST_NAME");

            // p.gender will be set to the default value "unknown"
            // when the column is null:
            p.gender = v.get<std::string>("GENDER", "unknown");

            // alternatively, the indicator can be tested directly:
            // if (v.indicator("GENDER") == i_null)
            // {
            //     p.gender = "unknown";
            // }
            // else
            // {
            //     p.gender = v.get<std::string>("GENDER");
            // }
        }
    
        static void to_base(const Person2 & p, values & v, indicator & ind)
        {
            v.set("ID", p.id);
            v.set("FIRST_NAME", p.firstName);
            v.set("LAST_NAME", p.lastName);
            v.set("GENDER", p.gender, p.gender.empty() ? i_null : i_ok);
            ind = i_ok;
        }
    };
}

#endif	/* PERSON2_H */

